import React from 'react';
import styled from '@emotion/styled';

function CloseButton(props) {
	return <StyledCloseButton color={props.color}>{props.children}</StyledCloseButton>;
}

const StyledCloseButton = styled('button')`
	background-color: blue;
	width: 100px;
	height: 60px;
	color: ${(props) => props.color};
`;

export default CloseButton;
