import styled from '@emotion/styled/macro';
import GlobalStyles from './designSystem/GlobalStyles';
import ConcentrationView from './components/ConcentrationView';
import PauseView from './components/PauseView';
import { useState } from 'react';

function App() {
	// mode = 'concentration' or 'pause'
	const [mode, setMode] = useState('concentration');

	return (
		<StyledApp mode={mode}>
			<GlobalStyles />
			<PauseView className="pause-view" />
			<ConcentrationView
				className="start-view"
				mode={mode}
				onComplete={() => {
					setMode('pause');
				}}
			/>
		</StyledApp>
	);
}

const StyledApp = styled('div')`
	width: 100%;
	height: 100%;
	text-align: center;
	position: relative;
	overflow: hidden;

	header {
		min-height: 100px;
		display: flex;
		flex-direction: column;
		align-items: center;
		justify-content: center;
		font-size: 40px;
	}

	.start-view {
		position: absolute;
		top: 0;
		left: 0;
		z-index: ${(props) => (props.mode === 'concentration' ? 1 : 0)};
	}

	.pause-view {
		position: absolute;
		top: 0;
		left: 0;
		z-index: ${(props) => (props.mode === 'pause' ? 1 : 0)};
	}
`;

export default App;
