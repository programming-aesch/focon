import * as React from 'react';
import { Global, css } from '@emotion/react/macro';
import { normalize } from 'polished';
import theme from './theme';

const GlobalStyles = () => (
	<>
		<Global
			styles={css`
				${normalize()}
			`}
		/>
		<Global
			styles={css`
				html,
				body {
					height: 100%;
					min-height: 500px;
					min-width: 375px;
					font-size: 16px;
					font-family: -apple-system, BlinkMacSystemFont, 'Segoe UI', 'Roboto', 'Oxygen', 'Ubuntu', 'Cantarell',
						'Fira Sans', 'Droid Sans', 'Helvetica Neue', sans-serif;
					font-weight: 400;
					line-height: ${20 / 16};
					background-color: #ffffff;
					-webkit-font-smoothing: antialiased;
					-moz-osx-font-smoothing: grayscale;
					color: #000000;
					-webkit-tap-highlight-color: rgba(255, 255, 255, 0);
					touch-action: manipulation;
				}

				#root {
					height: 100%;
				}

				a {
					color: ${theme.colors.highlight};
					text-decoration: none;
					cursor: pointer;

					&:hover {
						text-decoration: underline;
					}
				}

				button,
				input[type='submit'],
				input[type='reset'] {
					background: none;
					color: inherit;
					border: none;
					padding: 0;
					font: inherit;
					cursor: pointer;
					outline: inherit;
				}
			`}
		/>
	</>
);

export default GlobalStyles;
