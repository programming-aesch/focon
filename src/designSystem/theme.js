const theme = {
	breakpoints: {
		phone: 375,
		tablet: 768,
		desktop: 992,
		large: 1200,
	},
	colors: {
		white: '#FFFFFF',
		black: '#000000',
		green: 'rgb(31, 165, 129)',
		red: 'rgb(174, 6, 6)',
	},
	dimensions: {
		contentPadding: '20px',
		contentPaddingBig: '35px',
	},
};

export default theme;
