import React, { useState, useEffect } from 'react';
import styled from '@emotion/styled/macro';
import theme from '../designSystem/theme';
import StartButton from './StartConcentrationButton';
import RemainingTime from './RemainingTime';
import { motion, useMotionValue, animate, useTransform } from 'framer-motion';

const ConcentrationView = (props) => {
	const [isStarted, setIsStarted] = useState(false);
	const [time, setTime] = useState(20);
	const animationValue = useMotionValue(1);
	const backgroundTranslateY = useTransform(animationValue, [1, 0], ['translateY(0%)', 'translateY(100%)']);

	const reset = () => {
		setTime(20);
		setIsStarted(false);
		animationValue.set(1);
	};

	useEffect(() => {
		if (isStarted) {
			animate(animationValue, 0, {
				duration: 5,
				ease: 'linear',
				onUpdate: (value) => {
					setTime(Math.round(20 * value));
				},
				onComplete: () => {
					props.onComplete();
					reset();
				},
			});
		}
	}, [isStarted]); // eslint-disable-line

	return (
		<StyledConcentrationView
			className={props.className}
			style={{
				transform: backgroundTranslateY,
			}}
		>
			<RemainingTime time={time} />

			{!isStarted ? (
				<StartButton
					onClick={() => {
						setIsStarted(true);
					}}
				/>
			) : null}
		</StyledConcentrationView>
	);
};

const StyledConcentrationView = styled(motion.div)`
	width: 100%;
	height: 100%;
	background-color: ${theme.colors.green};
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

export default ConcentrationView;
