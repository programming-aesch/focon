import React from 'react';
import styled from '@emotion/styled/macro';
import theme from '../designSystem/theme';
import StartPauseButton from './StartPauseButton';

const PauseView = (props) => {
	return (
		<StyledPauseView className={props.className}>
			<StartPauseButton onClick={() => {}} />
		</StyledPauseView>
	);
};

const StyledPauseView = styled('div')`
	width: 100%;
	height: 100%;
	background-color: ${theme.colors.red};
	display: flex;
	flex-direction: column;
	justify-content: center;
	align-items: center;
`;

export default PauseView;
