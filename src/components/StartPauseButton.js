import React from 'react';
import styled from '@emotion/styled/macro';

const StartPauseButton = (props) => {
	const handleClick = () => {
		props.onClick();
	};

	return <StyledStartPauseButton onClick={handleClick}>Pause</StyledStartPauseButton>;
};

const StyledStartPauseButton = styled('button')`
	width: 160px;
	height: 160px;
	border-radius: 50%;
	background-color: #3db258;
	transition: transform 0.1s;
	color: white;
	font-weight: bold;
	font-size: 30px;

	:hover {
		transform: scale(1.05);
	}
`;

export default StartPauseButton;
