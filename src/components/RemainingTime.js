import React from 'react';
import styled from '@emotion/styled/macro';

const RemainingTime = (props) => {
	const isSmallNumber = parseInt(props.time) < 5;
	return <StyledRemainingTime isSmallNumber={isSmallNumber}>{props.time} min</StyledRemainingTime>;
};

const StyledRemainingTime = styled('div')`
	position: absolute;
	left: 0;
	top: ${(props) => (props.isSmallNumber ? '-4%' : '1%')};
	width: 100%;
	color: white;
`;

export default RemainingTime;
